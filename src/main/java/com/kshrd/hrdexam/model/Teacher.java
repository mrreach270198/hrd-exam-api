package com.kshrd.hrdexam.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Teacher {

    String id;
    String fullName;
    String gender;
    String email;
    String username;
    String password;
    String profileImg;
    int status;
    String resetPasswordToken;
}
