package com.kshrd.hrdexam.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentRequest {
    String id;
    String fullName;
    String email;
    String password;
    int classId;
    String requestAt;
    String gender;
}
