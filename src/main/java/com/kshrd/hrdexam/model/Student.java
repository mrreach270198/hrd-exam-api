package com.kshrd.hrdexam.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {

    String id;
    String fullName;
    String gender;
    String email;
    String password;
    int generationId;
    String profileImg;
    int classId;
    String createAt;
    String resetPasswordToken;
}
