package com.kshrd.hrdexam.service.teacherService;

import com.kshrd.hrdexam.model.Student;
import com.kshrd.hrdexam.model.StudentRequest;
import com.kshrd.hrdexam.model.Teacher;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TeacherService {

    List<StudentRequest> getAllStuRequest();
    StudentRequest getReqStuById(String id);
    public Boolean insertAcceptedStu(Student student);
    public int getCurrentGenId();
    Boolean deleteAcceptedStuById(String id);
    Teacher loadTeacherByUsername(String username);
    public Teacher findByEmail(String email);
    public Teacher findByResetPasswordToken(String token);
    public void updateResetPasswordToken(String token, String email) throws Exception;
    public Teacher getByResetPasswordToken(String token);
    public void updatePassword(Teacher teacher, String newPassword);
}
