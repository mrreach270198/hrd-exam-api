package com.kshrd.hrdexam.service.teacherService;

import com.kshrd.hrdexam.model.Student;
import com.kshrd.hrdexam.model.StudentRequest;
import com.kshrd.hrdexam.model.Teacher;
import com.kshrd.hrdexam.repository.teacherRepository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherServiceImp implements TeacherService{

    @Autowired
    private TeacherRepository teacherRepository;

    @Override
    public List<StudentRequest> getAllStuRequest() {
        return teacherRepository.getAllStudentsRequest();
    }

    @Override
    public StudentRequest getReqStuById(String id) {
        return teacherRepository.getReqStuById(id);
    }

    @Override
    public Boolean insertAcceptedStu(Student student) {
        return teacherRepository.insertAcceptedStu(student);
    }

    @Override
    public int getCurrentGenId() {
        return teacherRepository.getCurrentGenId();
    }

    @Override
    public Boolean deleteAcceptedStuById(String id) {
        return teacherRepository.deleteAcceptedStuById(id);
    }

    @Override
    public Teacher loadTeacherByUsername(String username) {
        return teacherRepository.loadTeacherByUsername(username);
    }

    @Override
    public Teacher findByEmail(String email) {
        return teacherRepository.findByEmail(email);
    }

    @Override
    public Teacher findByResetPasswordToken(String token) {
        return teacherRepository.findByResetPasswordToken(token);
    }




    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @Override
    public void updateResetPasswordToken(String token, String email) throws Exception {
        Teacher teacher = teacherRepository.findByEmail(email);
        if (teacher != null) {
            teacher.setResetPasswordToken(token);
            teacherRepository.save(teacher, teacher.getId());
        } else {
            throw new Exception("Could not find any user with the email " + email);
        }
    }

    @Override
    public Teacher getByResetPasswordToken(String token) {
        return teacherRepository.findByResetPasswordToken(token);
    }




    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @Override
    public void updatePassword(Teacher teacher, String newPassword) {

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(newPassword);
        teacher.setPassword(encodedPassword);

        teacher.setResetPasswordToken(null);
        teacherRepository.save(teacher, teacher.getId());

    }
}
