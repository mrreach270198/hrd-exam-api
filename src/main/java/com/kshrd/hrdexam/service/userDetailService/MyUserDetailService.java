package com.kshrd.hrdexam.service.userDetailService;

import com.kshrd.hrdexam.model.Student;
import com.kshrd.hrdexam.model.Teacher;
import com.kshrd.hrdexam.repository.studentRepository.StudentRepository;
import com.kshrd.hrdexam.repository.teacherRepository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private TeacherRepository teacherRepository;



    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Student student = studentRepository.loadStudentById(username);

        Teacher teacher = teacherRepository.loadTeacherByUsername(username);

        if(student != null && teacher == null) {
            return new CustomStudentDetail(student);
        }else if (teacher != null && student == null){
            return new CustomTeacherDetail(teacher);
        }else {
            throw new UsernameNotFoundException("User not found");
        }
    }
}
