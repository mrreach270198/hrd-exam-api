package com.kshrd.hrdexam.service.studentService;

import com.kshrd.hrdexam.model.Student;
import com.kshrd.hrdexam.model.StudentRequest;
import com.kshrd.hrdexam.model.Teacher;
import com.kshrd.hrdexam.repository.studentRepository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImp implements StudentService{

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public Boolean registerRequest(StudentRequest request) {
        return studentRepository.registerRequest(request);
    }

    @Override
    public int getClassIdByClassName(String name) {
        return studentRepository.getClassIdByClassName(name);
    }

    @Override
    public Student loadStudentById(String stuId) {
        return studentRepository.loadStudentById(stuId);
    }

    @Override
    public Student findByEmail(String email) {
        return studentRepository.findByEmail(email);
    }

    @Override
    public Student findByResetPasswordToken(String token) {
        return studentRepository.findByResetPasswordToken(token);
    }




    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//
    @Override
    public void updateResetPasswordToken(String token, String email) throws Exception {

        Student student = studentRepository.findByEmail(email);
        if (student != null) {
            student.setResetPasswordToken(token);
            studentRepository.save(student, student.getId());
        } else {
            throw new Exception("Could not find any user with the email " + email);
        }
    }

    @Override
    public Student getByResetPasswordToken(String token) {
        return studentRepository.findByResetPasswordToken(token);
    }



    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @Override
    public void updatePassword(Student student, String newPassword) {

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(newPassword);
        student.setPassword(encodedPassword);

        student.setResetPasswordToken(null);
        studentRepository.save(student, student.getId());

    }
}
