package com.kshrd.hrdexam.service.studentService;

import com.kshrd.hrdexam.model.Student;
import com.kshrd.hrdexam.model.StudentRequest;
import com.kshrd.hrdexam.model.Teacher;
import org.apache.ibatis.annotations.Param;

public interface StudentService {

    public Boolean registerRequest(StudentRequest request);
    public int getClassIdByClassName(String name);
    Student loadStudentById(String stuId);
    public Student findByEmail(String email);
    public Student findByResetPasswordToken(String token);
    public void updateResetPasswordToken(String token, String email) throws Exception;
    public Student getByResetPasswordToken(String token);
    public void updatePassword(Student student, String newPassword);
}
