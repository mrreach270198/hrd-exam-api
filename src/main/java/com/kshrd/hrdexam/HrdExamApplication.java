package com.kshrd.hrdexam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HrdExamApplication {

    public static void main(String[] args) {
        SpringApplication.run(HrdExamApplication.class, args);
    }

}
