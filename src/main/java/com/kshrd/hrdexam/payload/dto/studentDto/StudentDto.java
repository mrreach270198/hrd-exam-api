package com.kshrd.hrdexam.payload.dto.studentDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentDto {

    private String id;
    private String fullName;
    private String token;
    private String email;
}
