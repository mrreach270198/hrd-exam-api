package com.kshrd.hrdexam.payload.dto.teacherDto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeacherDto {

    private String username;
    private String fullName;
    private String token;
    private String email;
}
