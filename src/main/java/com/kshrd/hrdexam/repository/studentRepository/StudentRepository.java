package com.kshrd.hrdexam.repository.studentRepository;


import com.kshrd.hrdexam.model.Student;
import com.kshrd.hrdexam.model.StudentRequest;
import com.kshrd.hrdexam.model.Teacher;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository {

    @Insert("INSERT INTO student_request (student_id, full_name, email, password, class_id, request_at, gender) values(#{request.id},#{request.fullName},#{request.email},#{request.password},#{request.classId},#{request.requestAt},#{request.gender})")
    public Boolean registerRequest(@Param("request") StudentRequest request);

    @Select("SELECT id FROM class WHERE class_name=#{name}")
    public int getClassIdByClassName(String name);

    @Select("select *from students where id = #{stuId}")
    @Result(property = "fullName", column = "full_name")
    Student loadStudentById(String stuId);

    @Select("SELECT *FROM students WHERE email = #{email}")
    public Student findByEmail(@Param("email") String email);

    @Select("SELECT *FROM students WHERE reset_password_token = #{token}")
    public Student findByResetPasswordToken(String token);

    @Update("update students set password=#{student.password}, reset_password_token = #{student.resetPasswordToken}  where id=#{userId}")
    Boolean save(Student student, String userId);
}
