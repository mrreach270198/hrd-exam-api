package com.kshrd.hrdexam.repository.teacherRepository;


import com.kshrd.hrdexam.model.Student;
import com.kshrd.hrdexam.model.StudentRequest;
import com.kshrd.hrdexam.model.Teacher;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeacherRepository {

    @Select("SELECT *FROM student_request")
    @Result(property = "id", column = "student_id")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "requestAt", column = "request_at")
    @Result(property = "classId", column = "class_id")
    List<StudentRequest> getAllStudentsRequest();

    @Select("SELECT *FROM student_request WHERE student_id = #{id}")
    @Result(property = "id", column = "student_id")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "requestAt", column = "request_at")
    @Result(property = "classId", column = "class_id")
    StudentRequest getReqStuById(String id);

    @Insert("INSERT INTO students (id, full_name, gender, email, password,generation_id, profile_image, class_id, create_at) " +
            "values(#{student.id},#{student.fullName},#{student.gender},#{student.email},#{student.password},#{student.generationId},#{student.profileImg},#{student.classId},#{student.createAt})")
    public Boolean insertAcceptedStu(@Param("student")Student student);

    @Select("SELECT id FROM generations WHERE status='1'")
    public int getCurrentGenId();

    @Delete("DELETE FROM student_request WHERE student_id=#{id}")
    Boolean deleteAcceptedStuById(String id);

    @Select("select *from teachers where username = #{tcUsername}")
    @Result(property = "fullName", column = "full_name")
    Teacher loadTeacherByUsername(String tcUsername);


    @Select("SELECT *FROM teachers WHERE email = #{email}")
    public Teacher findByEmail(@Param("email") String email);

    @Select("SELECT *FROM teachers WHERE reset_password_token = #{token}")
    public Teacher findByResetPasswordToken(String token);

    @Update("update teachers set password=#{teacher.password}, reset_password_token = #{teacher.resetPasswordToken}  where id=#{userId}")
    Boolean save(Teacher teacher, String userId);
}
