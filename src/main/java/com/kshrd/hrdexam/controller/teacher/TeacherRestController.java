package com.kshrd.hrdexam.controller.teacher;


import com.kshrd.hrdexam.model.Student;
import com.kshrd.hrdexam.model.StudentRequest;
import com.kshrd.hrdexam.model.Teacher;
import com.kshrd.hrdexam.payload.dto.studentDto.StudentDto;
import com.kshrd.hrdexam.payload.dto.teacherDto.TeacherDto;
import com.kshrd.hrdexam.payload.request.studentRequest.RegisterRequest;
import com.kshrd.hrdexam.payload.request.studentRequest.StudentLoginRequest;
import com.kshrd.hrdexam.payload.request.teacherRequest.TeacherLoginRequest;
import com.kshrd.hrdexam.security.jwt.JwtUtil;
import com.kshrd.hrdexam.service.teacherService.TeacherServiceImp;
import com.kshrd.hrdexam.service.userDetailService.MyUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/teacher")
public class TeacherRestController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MyUserDetailService userDetailService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private TeacherServiceImp teacherServiceImp;



    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @GetMapping("/getAllStudentsRequest")
    public ResponseEntity<?> getAllStudentRequest(){

        List<StudentRequest> studentRequestList = teacherServiceImp.getAllStuRequest();

        Map<String, Object> response = new HashMap<>();

        if (!studentRequestList.isEmpty()){
            response.put("status", "OK");
            response.put("message", "get all student request success!");
            response.put("success", true);
            response.put("payload", studentRequestList);
        }else {
            response.put("status", "OK");
            response.put("message", "No student request");
            response.put("payload", "null");
        }
        return ResponseEntity.ok().body(response);
    }




    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @GetMapping("/acceptStudentRequestById/{id}")
    public ResponseEntity<?> acceptStudentRequestById(@PathVariable String id){

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
        Date date = new Date();

        StudentRequest studentRequest = teacherServiceImp.getReqStuById(id);

        Student student = new Student();
        student.setId(studentRequest.getId());
        student.setFullName(studentRequest.getFullName());
        student.setGender(studentRequest.getGender());
        student.setEmail(studentRequest.getEmail());
        student.setPassword(studentRequest.getPassword());
        student.setGenerationId(teacherServiceImp.getCurrentGenId());
        student.setProfileImg("default.png");
        student.setClassId(studentRequest.getClassId());
        student.setCreateAt(formatter.format(date));

        Map<String, Object> response = new HashMap<>();

        if (teacherServiceImp.insertAcceptedStu(student) && teacherServiceImp.deleteAcceptedStuById(student.getId())){
            response.put("status", "OK");
            response.put("message", "accept student request success!");
            response.put("success", true);
            response.put("payload", studentRequest);
        }else {
            response.put("status", "ERROR");
            response.put("message", "accept student request fail!");
            response.put("success", false);
            response.put("payload", "null");
        }
        return ResponseEntity.ok().body(response);
    }



    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @PostMapping("/login")
    public ResponseEntity<?> loginTeacher(@RequestBody TeacherLoginRequest loginRequest) throws Exception {

        Map<String, Object> response = new HashMap<>();

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect username password", e);
        }

        final UserDetails userDetails = userDetailService
                .loadUserByUsername(loginRequest.getUsername());
        final String jwt = jwtUtil.generateToken(userDetails);

        Teacher teacher = teacherServiceImp.loadTeacherByUsername(loginRequest.getUsername());
        System.out.println("teacher "+teacher);

        TeacherDto teacherDto = new TeacherDto();

        teacherDto.setUsername(teacher.getUsername());
        teacherDto.setFullName(teacher.getFullName());
        teacherDto.setEmail(teacher.getEmail());
        teacherDto.setToken(jwt);

        response.put("status", "OK");
        response.put("message", "login success");
        response.put("success", true);
        response.put("payload", teacherDto);

        return ResponseEntity.ok(response);
    }

}
