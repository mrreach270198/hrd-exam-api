package com.kshrd.hrdexam.controller.student;

import com.kshrd.hrdexam.model.Student;
import com.kshrd.hrdexam.model.StudentRequest;
import com.kshrd.hrdexam.payload.dto.studentDto.StudentDto;
import com.kshrd.hrdexam.payload.request.studentRequest.RegisterRequest;
import com.kshrd.hrdexam.payload.request.studentRequest.StudentLoginRequest;
import com.kshrd.hrdexam.security.jwt.JwtUtil;
import com.kshrd.hrdexam.service.studentService.StudentServiceImp;
import com.kshrd.hrdexam.service.userDetailService.MyUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/student")
public class StudentRestController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MyUserDetailService userDetailService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private StudentServiceImp studentServiceImp;

    @Autowired
    private PasswordEncoder passwordEncoder;


    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody RegisterRequest request){

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
        Date date = new Date();

        int classId = studentServiceImp.getClassIdByClassName(request.getClassName());

        StudentRequest studentRequest = new StudentRequest();

        studentRequest.setId(request.getId());
        studentRequest.setFullName(request.getFullName());
        studentRequest.setEmail(request.getEmail());
        studentRequest.setPassword(passwordEncoder.encode(request.getPassword()));
        studentRequest.setClassId(classId);
        studentRequest.setRequestAt(formatter.format(date));
        studentRequest.setGender(request.getGender());

        Map<String, Object> response = new HashMap<>();

        if (studentServiceImp.registerRequest(studentRequest)){
            response.put("status", "OK");
            response.put("message", "register success!");
            response.put("success", true);
            response.put("payload", request);
        }else {
            response.put("status", "ERROR");
            response.put("message", "register fail!");
            response.put("success", false);
            response.put("payload", "null");
        }

        return ResponseEntity.ok().body(response);
    }




    //========================================================================================//
    //                                 Login Method for student                               //
    //========================================================================================//
    //   In the method parameter have RequestBody as StudentLoginRequest that has 2 fields    //
    //   -String id, String password.                                                         //
    //   + we use  authenticationManager.authenticate(                                        //
    //                    new UsernamePasswordAuthenticationToken(id, password)               //
    //     to verify student login with id and password.                                      //
    //   + we use final UserDetails userDetails = userDetailService                           //
    //                .loadUserByUsername(loginRequest.getId());                              //
    //     to get data of student that login from database as UserDetails                     //                                                            //
    //                                                                                        //
    //****************************************************************************************//

    @PostMapping("/login")
    public ResponseEntity<?> loginStudent(@RequestBody StudentLoginRequest loginRequest) throws Exception {

        Map<String, Object> response = new HashMap<>();

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getId(), loginRequest.getPassword()));
        } catch (BadCredentialsException e) {
            response.put("message", "Incorrect username password");
            throw new Exception("Incorrect username password", e);
        }

        final UserDetails userDetails = userDetailService
                .loadUserByUsername(loginRequest.getId());
        final String jwt = jwtUtil.generateToken(userDetails);

        Student student = studentServiceImp.loadStudentById(loginRequest.getId());

        StudentDto studentDto = new StudentDto();

        studentDto.setId(student.getId());
        studentDto.setFullName(student.getFullName());
        studentDto.setEmail(student.getEmail());
        studentDto.setToken(jwt);

        response.put("status", "OK");
        response.put("message", "login success");
        response.put("success", true);
        response.put("payload", studentDto);

        return ResponseEntity.ok(response);
    }
}
