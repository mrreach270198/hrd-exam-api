package com.kshrd.hrdexam.controller.forgetpassword;

import com.kshrd.hrdexam.model.Student;
import com.kshrd.hrdexam.model.Teacher;
import com.kshrd.hrdexam.service.studentService.StudentServiceImp;
import com.kshrd.hrdexam.service.teacherService.TeacherServiceImp;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ResetPassword {

    @Autowired
    private TeacherServiceImp teacherServiceImp;

    @Autowired
    private StudentServiceImp studentServiceImp;

    @GetMapping("/login")
    String home(){

        return "index";
    }




    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @GetMapping("/teacher/reset_password")
    public String teacherShowResetPasswordForm(@Param(value = "token") String token, Model model) {
        Teacher teacher = teacherServiceImp.getByResetPasswordToken(token);
        model.addAttribute("token", token);

        if (teacher == null) {
            model.addAttribute("message", "Invalid Token");
            return "message";
        }

        return "teacher_reset_password_form";
    }



    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @PostMapping("/teacher/reset_password")
    public String teacherProcessResetPassword(HttpServletRequest request, Model model) {
        String token = request.getParameter("token");
        String password = request.getParameter("password");

        Teacher teacher = teacherServiceImp.getByResetPasswordToken(token);
        model.addAttribute("title", "Reset your password");

        if (teacher == null) {
            model.addAttribute("message", "Invalid Token");
            return "message";
        } else {
            teacherServiceImp.updatePassword(teacher, password);

            model.addAttribute("message", "You have successfully changed your password.");
        }

        return "message";
    }



    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @GetMapping("/student/reset_password")
    public String studentShowResetPasswordForm(@Param(value = "token") String token, Model model) {
        Student student = studentServiceImp.getByResetPasswordToken(token);
        model.addAttribute("token", token);

        if (student == null) {
            model.addAttribute("message", "Invalid Token");
            return "message";
        }

        return "student_reset_password_form";
    }



    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @PostMapping("/student/reset_password")
    public String studentProcessResetPassword(HttpServletRequest request, Model model) {
        String token = request.getParameter("token");
        String password = request.getParameter("password");

        Student student = studentServiceImp.getByResetPasswordToken(token);
        model.addAttribute("title", "Reset your password");

        if (student == null) {
            model.addAttribute("message", "Invalid Token");
            return "message";
        } else {
            studentServiceImp.updatePassword(student, password);

            model.addAttribute("message", "You have successfully changed your password.");
        }

        return "message";
    }
}
