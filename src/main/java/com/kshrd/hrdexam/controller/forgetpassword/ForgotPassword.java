package com.kshrd.hrdexam.controller.forgetpassword;

import com.kshrd.hrdexam.service.studentService.StudentServiceImp;
import com.kshrd.hrdexam.service.teacherService.TeacherServiceImp;
import org.modelmapper.internal.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/forgotPassword")
public class ForgotPassword {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private TeacherServiceImp teacherServiceImp;

    @Autowired
    private StudentServiceImp studentServiceImp;

    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @PostMapping("/teacher")
    public ResponseEntity<?> teacherProcessForgotPassword(@RequestParam String email) {

        Map<String, Object> response = new HashMap<>();

        String token = RandomString.make(30);

        try {
            teacherServiceImp.updateResetPasswordToken(token, email);
            String resetPasswordLink = Utility.getSiteURL(request) + "/teacher/reset_password?token=" + token;
            sendEmail(email, resetPasswordLink);
            response.put("email", email);
            response.put("message", "We have send you a reset password link. Please check your email.");
            response.put("status", "OK");

        } catch (Exception ex) {
            response.put("email", "error");
        }

        return ResponseEntity.ok().body(response);
    }



    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @PostMapping("/student")
    public ResponseEntity<?> studentProcessForgotPassword(@RequestParam String email) {

        Map<String, Object> response = new HashMap<>();

        String token = RandomString.make(30);

        try {
            studentServiceImp.updateResetPasswordToken(token, email);
            String resetPasswordLink = Utility.getSiteURL(request) + "/student/reset_password?token=" + token;
            sendEmail(email, resetPasswordLink);
            response.put("email", email);
            response.put("message", "We have send you a reset password link. Please check your email.");
            response.put("status", "OK");

        } catch (Exception ex) {
            response.put("email", "error");
        }

        return ResponseEntity.ok().body(response);
    }




    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//


    public void sendEmail(String recipientEmail, String link)
            throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("contact@kshrd.com", "HRD Exam Support");
        helper.setTo(recipientEmail);

        String subject = "Here's the link to reset your password";

        String content = "<p>Hello,</p>"
                + "<p>You have requested to reset your password.</p>"
                + "<p>Click the link below to change your password:</p>"
                + "<p><a href=\"" + link + "\">Change my password</a></p>"
                + "<br>"
                + "<p>Ignore this email if you do remember your password, "
                + "or you have not made the request.</p>";

        helper.setSubject(subject);

        helper.setText(content, true);

        mailSender.send(message);
    }
}
